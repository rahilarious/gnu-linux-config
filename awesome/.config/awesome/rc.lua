-- Standard awesome library
local gears = require("gears")
local awful = require("awful")
-- focus clients when swiching tags, so it's important
require("awful.autofocus")
-- Widget and layout library
local wibox = require("wibox")
-- Theme handling library
local beautiful = require("beautiful")
-- Notification library
local naughty = require("naughty")
local lain = require("lain")


-- ####### Error handling
-- Check if awesome encountered an error during startup and fell back to
-- another config (This code will only ever execute for the fallback config)
if awesome.startup_errors then
    naughty.notify({ preset = naughty.config.presets.critical,
                     title = "Oops, there were errors during startup!",
                     text = awesome.startup_errors })
end

-- Handle runtime errors after startup
do
    local in_error = false
    awesome.connect_signal("debug::error", function (err)
        -- Make sure we don't go into an endless error loop
        if in_error then return end
        in_error = true

        naughty.notify({ preset = naughty.config.presets.critical,
                         title = "Oops, an error happened!",
                         text = tostring(err) })
        in_error = false
    end)
end
-- #######

-- ####### Variable definitions
-- Themes define colours, icons, font and wallpapers.
beautiful.init("~/.config/awesome/themes/osmtheme/theme.lua")

terminal = "alacritty"
editor = os.getenv("EDITOR") or "emacs -nw"
editor_cmd = terminal .. " -e " .. editor

modkey = "Mod4"

-- Table of layouts to cover with awful.layout.inc, order matters.
awful.layout.layouts = {
    awful.layout.suit.tile.bottom,
    awful.layout.suit.tile,
    awful.layout.suit.max,
    awful.layout.suit.fair.horizontal,
    -- awful.layout.suit.tile.left,
    -- awful.layout.suit.tile.top,
    awful.layout.suit.fair,
    -- awful.layout.suit.spiral,
    -- awful.layout.suit.spiral.dwindle,
    -- awful.layout.suit.max.fullscreen,
    awful.layout.suit.floating,
    -- awful.layout.suit.magnifier,
    -- awful.layout.suit.corner.nw,
    -- awful.layout.suit.corner.ne,
    -- awful.layout.suit.corner.sw,
    -- awful.layout.suit.corner.se,
}

-- Create a wibox for each screen and add it
local taglist_buttons = gears.table.join(
		    -- left click to focus, mod+left will move to that tag
                    awful.button({ }, 1, function(t) t:view_only() end),
                    awful.button({ modkey }, 1, function(t)
                                              if client.focus then
                                                  client.focus:move_to_tag(t)
                                              end
                                          end),
		    -- right click to toggle tags, mod+right click to copy to tag
		    awful.button({ }, 3, awful.tag.viewtoggle),
                    awful.button({ modkey }, 3, function(t)
                                              if client.focus then
                                                  client.focus:toggle_tag(t)
                                              end
                                          end),
		    -- scroll up and down to switch workspaces/tags whatever you wanna call it
                    awful.button({ }, 4, function(t) awful.tag.viewnext(t.screen) end),
                    awful.button({ }, 5, function(t) awful.tag.viewprev(t.screen) end)
                )

local tasklist_buttons = gears.table.join(
		     -- left click focuses
                     awful.button({ }, 1, function (c)
                                              if c == client.focus then
                                                  c.minimized = true
                                              else
                                                  -- Without this, the following
                                                  -- :isvisible() makes no sense
                                                  c.minimized = false
                                                  if not c:isvisible() and c.first_tag then
                                                      c.first_tag:view_only()
                                                  end
                                                  -- This will also un-minimize
                                                  -- the client, if needed
                                                  client.focus = c
                                                  c:raise()
                                              end
                                          end),
		     awful.button({ }, 2, function (c) c:kill() end),
		     -- right click shows active windows/tasks
                     -- awful.button({ }, 3, client_menu_toggle_fn()),
		     -- scroll up and down to switch tasks
                     awful.button({ }, 4, function ()
                                              awful.client.focus.byidx(1)
                                          end),
                     awful.button({ }, 5, function ()
                                              awful.client.focus.byidx(-1)
                                          end))


local tagnames = { "1", "2", "3", "4", "5", "6", "", "", "" }
awful.screen.connect_for_each_screen(function(s)
    -- Each screen has its own tag table.
      awful.tag({ (tagnames[1]), (tagnames[2]), (tagnames[3]), (tagnames[4]), (tagnames[5]), (tagnames[6]) }, s, awful.layout.layouts[1])

      awful.tag.add(tagnames[7], {
		       layout = awful.layout.layouts[3]})
      awful.tag.add(tagnames[8], { screen = s, layout = awful.layout.layouts[1] })
      awful.tag.add(tagnames[9], { screen = s, layout = awful.layout.layouts[1] })

    s.mypromptbox = awful.widget.prompt()
    -- Create an imagebox widget which will contain an icon indicating which layout we're using.
    -- We need one layoutbox per screen.
    s.mylayoutbox = awful.widget.layoutbox(s)
    s.mylayoutbox:buttons(gears.table.join(
			   -- left, right, scroll up and down
                           awful.button({ }, 1, function () awful.layout.inc( 1) end),
                           awful.button({ }, 3, function () awful.layout.inc(-1) end),
                           awful.button({ }, 4, function () awful.layout.inc( 1) end),
                           awful.button({ }, 5, function () awful.layout.inc(-1) end)))
    -- Create a taglist widget
    -- s.mytaglist = awful.widget.taglist(s, awful.widget.taglist.filter.all, taglist_buttons)

    s.mytaglist = awful.widget.taglist {screen =  s, filter  = awful.widget.taglist.filter.all, buttons = taglist_buttons,
    widget_template = {
        {
            {
                {
                    id     = 'text_role',
                    widget = wibox.widget.textbox,
                },
                layout = wibox.layout.fixed.horizontal,
            },
            left  = 14,
            right = 14,
            widget = wibox.container.margin
        },
        id     = 'background_role',
        widget = wibox.container.background,
    },
    }

    -- Create a tasklist widget
    s.mytasklist = awful.widget.tasklist(s, awful.widget.tasklist.filter.currenttags, tasklist_buttons)

    -- Create the wibox
    s.mywibox = awful.wibar({ position = "bottom", screen = s, opacity = 0.9 })
    s.mywibox.visible = false


    -- ####### Wibar
    -- Create a textclock widget
    mytextclock = wibox.widget.textclock("<span color='#ff5252'> </span>" .." %d-%m %a" .. "<span color='#fbc02d'> </span>" .. " %I:%M %p ", 10)

    -- Calendar
    beautiful.cal = lain.widget.cal({
        attach_to = { mytextclock },
        week_number = "right",
        notification_preset = {
            font = "Monospace 14",
            fg   = beautiful.fg_normal,
            bg   = beautiful.bg_normal
        }
    })    

   local cpu = lain.widget.cpu({
       settings = function()
           widget:set_markup(lain.util.markup.fontfg(beautiful.font, "#e33a6e", "  " .. cpu_now.usage .. "% "))
       end,       timeout = 2
   })
    
    local temp = lain.widget.temp({
        settings = function()
            widget:set_markup(lain.util.markup.fontfg(beautiful.font, "#f1af5f", "  " .. coretemp_now .. "°C "))
        end,        tempfile = "/sys/devices/platform/coretemp.0/hwmon/hwmon2/temp1_input",
        timeout = 2
    })
    
    local netdowninfo = wibox.widget.textbox()
    local netupinfo = lain.widget.net({
        settings = function()
            --[[ uncomment if using the weather widget
            if iface ~= "network off" and
            string.match(theme.weather.widget.text, "N/A")
            then
                theme.weather.update()
            end
            --]]
            
            widget:set_markup(" <span color='#c37dfd'></span>" .. "  " .. net_now.sent .. " KiB " .. "  " .. net_now.received .. " KiB ")
            -- netdowninfo:set_markup("  " .. net_now.received .. " KiB ")
        end,        timeout = 2, })
    
   local memory = lain.widget.mem({
       settings = function()
           widget:set_markup(lain.util.markup.fontfg(beautiful.font, "#e0da37","  " .. mem_now.perc .. "% "))
       end,       timeout = 2
   })

   local change_wallpaper = wibox.widget.textbox()
   change_wallpaper.markup = "<span color='#00c853'> </span> "
   change_wallpaper:buttons(gears.table.join(
			       awful.button({},1, function() awful.spawn.with_shell("~/.fehbg") end)
   ))
    
    
    -- Add widgets to the wibox
    s.mywibox:setup {
        layout = wibox.layout.align.horizontal,
        { -- Left widgets
            layout = wibox.layout.fixed.horizontal,
            s.mytaglist,
            s.mylayoutbox,
	    s.mypromptbox,
        },
        s.mytasklist, -- Middle widget
        { -- Right widgets
            layout = wibox.layout.fixed.horizontal,
            netupinfo.widget,
            netdowninfo,
            -- memory.widget,
            -- cpu.widget,
            -- temp.widget,
            mytextclock,
	    change_wallpaper,
        },
    }
end)
-- #######
-- No borders when rearranging only 1 non-floating or maximized client
screen.connect_signal("arrange", function(s)
    local only_one = #s.tiled_clients == 1
    for _, c in pairs(s.clients) do
        if only_one and not c.floating or c.maximized then
            c.border_width = 0
        else
            c.border_width = beautiful.border_width
        end
    end
end)

-- ####### Mouse bindings
-- root.buttons(gears.table.join(
--     awful.button({ }, 4, awful.tag.viewnext),
--     awful.button({ }, 5, awful.tag.viewprev)
-- ))
-- #######

-- ####### Key bindings
globalkeys = gears.table.join(
    awful.key({ modkey,           }, "j",
        function ()
            awful.client.focus.byidx( 1)
        end),
    awful.key({ modkey,           }, "k",
        function ()
            awful.client.focus.byidx(-1)
        end        
    ),

    awful.key({ modkey, "Control"    }, "h",   awful.tag.viewprev
              ),
    awful.key({ modkey,         "Control"  }, "l",  awful.tag.viewnext
              ),

    -- Layout manipulation
    awful.key({ modkey, "Shift"   }, "j", function () awful.client.swap.byidx(  1)    end              ),
    awful.key({ modkey, "Shift"   }, "k", function () awful.client.swap.byidx( -1)    end              ),
    awful.key({ modkey, "Control" }, "j", function () awful.screen.focus_relative( 1) end              ),
    awful.key({ modkey, "Control" }, "k", function () awful.screen.focus_relative(-1) end              ),
    awful.key({ modkey,           }, "u", awful.client.urgent.jumpto
              ),

    -- Standard program
    awful.key({ modkey,           }, "Return", function () awful.spawn(terminal) end              ),
    awful.key({ modkey, "Shift" }, "r", awesome.restart
              ),
    awful.key({ modkey , "Shift"   }, "q", awesome.quit
              ),

    awful.key({ modkey,           }, "l",     function () awful.tag.incmwfact( 0.05)          end              ),
    awful.key({ modkey,           }, "h",     function () awful.tag.incmwfact(-0.05)          end              ),
    awful.key({ modkey,           }, "s", function () awful.layout.inc( 1)                end              ),
    awful.key({ modkey, "Shift"   }, "s", function () awful.layout.inc(-1)                end              ),

    awful.key({ modkey, "Control" }, "n",
              function ()
                  local c = awful.client.restore()
                  -- Focus restored client
                  if c then
                      client.focus = c
                      c:raise()
                  end
              end              ),

	
    awful.key({ modkey }, "y",
       function (c)
	  local cmd = "st -c scratchpad"
	  local rule = { class = "scratchpad" }
	  
	  if client.focus and awful.rules.match(client.focus,  rule) then
	     local ctags = {}
	     for k,tag in pairs(client.focus:tags()) do
		if tag ~= awful.tag.selected(client.focus.screen) then table.insert(ctags, tag) end
	     end
	     client.focus:tags(ctags)
	  else
	     local findex  = awful.util.table.hasitem(client.get(), client.focus) or 1
	     local start   = awful.util.cycle(#client.get(), findex + 1)

	     for c in awful.client.iterate(function(c) return awful.rules.match(c, rule) end, start) do
		ctags = {awful.tag.selected(c.screen)}
		for k,tag in pairs(c:tags()) do
		   if tag ~= awful.tag.selected(c.screen) then table.insert(ctags, tag) end
		end
		c:tags(ctags)
		c:raise()
		client.focus = c
		return
	     end
	     -- client not found, spawn it
	     awful.util.spawn(cmd)
	  end
       end
    ),
    
	awful.key({modkey, "Control"}, "c", function() awful.spawn('firefox -P Cooku') end),
	awful.key({modkey, "Control"}, "e", function() awful.spawn('emacsclient -c') end),
	awful.key({modkey, "Control"}, "f", function() awful.spawn('firefox -P NiceBoi') end),
	awful.key({modkey, "Control"}, "g", function() awful.spawn('clipmenu') end),
	awful.key({modkey, "Control"}, "p", function() awful.spawn(terminal .. '-e pulsemixer') end),

	awful.key({},'XF86AudioRaiseVolume', function() awful.spawn.with_shell('pactl set-sink-volume @DEFAULT_SINK@ +10%') end),
	awful.key({},'XF86AudioLowerVolume', function() awful.spawn.with_shell('pactl set-sink-volume @DEFAULT_SINK@ -10%') end),
	awful.key({},'XF86AudioMute', function() awful.spawn.with_shell('pactl set-sink-mute @DEFAULT_SINK@ toggle') end),
	awful.key({},'XF86MonBrightnessUp', function() awful.spawn.with_shell('xbacklight -inc 5') end),
	awful.key({},'XF86MonBrightnessDown', function() awful.spawn.with_shell('xbacklight -dec 5') end),
	awful.key({},'XF86TouchpadToggle', function() awful.spawn.with_shell('~/.sexyscripts/toggletouchpad.sh') end),
	awful.key({modkey},'z', function() awful.spawn.with_shell('~/.sexyscripts/i3lock.sh') end),
	awful.key({},'Print', function() awful.spawn('flameshot gui') end),



	awful.key({ modkey, "Shift" }, "n", function () lain.util.rename_tag() end              ),

    -- Show/Hide Wibox
    awful.key({ modkey }, "b", function ()
            for s in screen do
                s.mywibox.visible = not s.mywibox.visible
                if s.mybottomwibox then
                    s.mybottomwibox.visible = not s.mybottomwibox.visible
                end
            end
        end        ),



    -- Prompt
    awful.key({ modkey },            "Tab",     function () awful.spawn("rofi -show run")  end              )

)

clientkeys = gears.table.join(
    awful.key({ modkey,           }, "f",
        function (c)
            c.fullscreen = not c.fullscreen
            c:raise()
        end        ),
    awful.key({ modkey 	  }, "q",      function (c) c:kill()                         end              ),
    awful.key({ modkey           }, "space",  awful.client.floating.toggle                     
              ),
    awful.key({ modkey, "Shift" }, "Return", function (c) c:swap(awful.client.getmaster()) end              ),
    awful.key({ modkey}, "o",      function (c) c:move_to_screen()               end              ),
    awful.key({ modkey           }, "n",
        function (c)
            -- The client currently has the input focus, so it cannot be
            -- minimized, since minimized clients can't have the focus.
            c.minimized = true
        end 
        ),
    awful.key({ modkey,           }, "m",
        function (c)
            c.maximized = not c.maximized
            c:raise()
        end 
        )
)

-- Bind all key numbers to tags.
-- Be careful: we use keycodes to make it work on any keyboard layout.
-- This should map on the top row of your keyboard, usually 1 to 9.
for i = 1, 9 do
    globalkeys = gears.table.join(globalkeys,
        -- View tag only.
        awful.key({ modkey }, "#" .. i + 9,
                  function ()
                        local screen = awful.screen.focused()
                        local tag = screen.tags[i]
                        if tag then
                           tag:view_only()
                        end
                  end                  ),
        -- Toggle tag display.
        awful.key({ modkey, "Control" }, "#" .. i + 9,
                  function ()
                      local screen = awful.screen.focused()
                      local tag = screen.tags[i]
                      if tag then
                         awful.tag.viewtoggle(tag)
                      end
                  end                  ),
        -- Move client to tag.
        awful.key({ modkey, "Shift" }, "#" .. i + 9,
                  function ()
                      if client.focus then
                          local tag = client.focus.screen.tags[i]
                          if tag then
                              client.focus:move_to_tag(tag)
                          end
                     end
                  end                  ),
        -- Toggle tag on focused client.
        awful.key({ modkey, "Control", "Shift" }, "#" .. i + 9,
                  function ()
                      if client.focus then
                          local tag = client.focus.screen.tags[i]
                          if tag then
                              client.focus:toggle_tag(tag)
                          end
                      end
                  end                  )
    )
end

clientbuttons = gears.table.join(
    awful.button({ }, 1, function (c) client.focus = c; c:raise() end),
    awful.button({ modkey }, 1, awful.mouse.client.move),
    awful.button({ modkey }, 3, awful.mouse.client.resize))

-- Set keys
root.keys(globalkeys)
-- #######

-- ####### Rules
-- Rules to apply to new clients (through the "manage" signal).
awful.rules.rules = {
    -- All clients will match this rule.
    { rule = { },
      properties = { border_width = beautiful.border_width,
                     border_color = beautiful.border_normal,
                     focus = awful.client.focus.filter,
                     raise = true,
                     keys = clientkeys,
                     buttons = clientbuttons,
                     screen = awful.screen.preferred,
                     placement = awful.placement.no_overlap+awful.placement.no_offscreen
     }
    },

    -- Floating clients.
    { rule_any = {
        instance = {
          "DTA",  -- Firefox addon DownThemAll.
          "copyq",  -- Includes session name in class.
        },
        class = {
          "Arandr",
          "Gpick",
          "Kruler",
          "MessageWin",  -- kalarm.
          "Sxiv",
	  "gcr-prompter",
          "Pavucontrol",
	  "scratchpad",
          "Wpa_gui",
          "pinentry",
          "veromix",
          "xtightvncviewer"},

        name = {
          "Event Tester",  -- xev.
        },
        role = {
          "AlarmWindow",  -- Thunderbird's calendar.
          "pop-up",       -- e.g. Google Chrome's (detached) Developer Tools.
        }
    }, properties = {
	 floating = true,
	 placement = awful.placement.centered,
    }},

    { rule_any = { class = {
		      "firefox",
		      "Nyxt",
		      "Surf",
		      "Chromium-bin-browser-chromium",
		 }},
      properties = { tag = tagnames[7] }
    },

    { rule_any = { class = {
		      "TelegramDesktop",
		      "Wire",
		 }},
      properties = { tag = tagnames[8] }
    },

}


-- Signal function to execute when a new client appears.
client.connect_signal("manage", function (c)
    -- Set the windows at the slave,
    -- i.e. put it at the end of others instead of setting it master.
    -- if not awesome.startup then awful.client.setslave(c) end

    if awesome.startup and
      not c.size_hints.user_position
      and not c.size_hints.program_position then
        -- Prevent clients from being unreachable after screen count changes.
        awful.placement.no_offscreen(c)
    end
end)


-- Enable sloppy focus, so that focus follows mouse.
client.connect_signal("mouse::enter", function(c)
    if awful.layout.get(c.screen) ~= awful.layout.suit.magnifier
        and awful.client.focus.filter(c) then
        client.focus = c
    end
end)

client.connect_signal("focus", function(c) c.border_color = beautiful.border_focus end)
client.connect_signal("unfocus", function(c) c.border_color = beautiful.border_normal end)








-- startup script
awful.spawn.with_shell("~/.wmrc")

