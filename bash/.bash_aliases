#!/bin/bash

# misc 
alias \
    bashaliases='emacsclient -t ~/.bash_aliases && . ~/.bashrc' \
    ..='cd ..' \
    ...='cd ../..' \
    ....='cd ../../..' \
    grep='command grep --color=auto' \
    egrep='command egrep --color=auto' \
    lsmount='mount | column -t' \
    ssh-add='eval $(ssh-agent) && ssh-add' \
    ls='ls --color=auto' \
    l='command eza -lbg --color=always' \
    la='command eza -labg --git --group-directories-first --color=always' \
    lad='command eza -labgD --git --color=always' \
    rm='rm -I' \
    diff='diff --color=always -U 1' \
    ec='emacsclient -t' \
    lspci='lspci -nn' \
    reload='. ~/.bashrc' \
    termapp='doas pkill -15 -i $1' \
    colemak='doas loadkeys en-latin9' \
    tvsetup='xrandr --output eDP-1 --primary --mode 1920x1080 --pos 0x0 --rotate normal --output HDMI-1-4 --pos 1920x0 --rotate normal' \
    tvsetup-wl='wlr-randr --output eDP-1 --mode 1920x1080 --preferred --pos 0,0 --output HDMI-A-4 --mode 1366x768 --pos 1920,0' \
    gen2off='doas systemctl poweroff' \
    gen2sleep='doas systemctl suspend' \
    myip='curl https://ifconfig.me/ip && echo' \
    searchkmod='read -p "Enter search term: "; lspci -k | grep -i "${REPLY}" && echo && echo && echo; lsmod | grep -i "${REPLY}"' \
    htop='command htop -d 10' \
    htop5='command htop -d 5' \
    infoarch='gcc -march=native -Q --help=target | grep -i march && /lib64/ld-linux-x86-64.so.2 --help | grep -i x86-64-v' \
    infointelmicrocode='iucode_tool -Sl /lib/firmware/intel-ucode/* | less' \
    nvprime='__NV_PRIME_RENDER_OFFLOAD=1 __GLX_VENDOR_LIBRARY_NAME=nvidia ' \
    mem5='ps auxf | sort -nr -k 4 | head -5' \
    mem10='ps auxf | sort -nr -k 4 | head -10' \
    cpu5='ps auxf | sort -nr -k 3 | head -5' \
    cpu10='ps auxf | sort -nr -k 3 | head -10' \
    dir5='du -csh * | sort -hr | head -n 5' \
    dir10='du -csh * | sort -hr | head -n 10' \
    gits='git status' \
    gitd='git difftool --no-symlinks --dir-diff' \


#system management
alias \
    wifidc='doas iwctl station wlan0 disconnect' \
    wifils='doas iwctl station wlan0 scan && sleep 2 && doas iwctl station wlan0 get-networks' \
    wifinc='doas iwctl station wlan0 scan && sleep 2 && doas iwctl station wlan0 get-networks && read -p "Enter wifi SSID: " mssid && read -p "Enter wifi password: " mpasswd && doas iwctl --passphrase "${mpasswd}" station wlan0 connect "${mssid}" && sleep 3 && doas iwctl station wlan0 show' \
    wifisc='doas iwctl station wlan0 scan && sleep 2 && doas iwctl station wlan0 get-networks && read -p "Enter wifi SSID: " mssid && doas iwctl station wlan0 connect "${mssid}" && sleep 3 && doas iwctl station wlan0 show' \
    wific='doas iwctl station wlan0 get-networks && read -p "Enter wifi SSID: " mssid && doas iwctl station wlan0 connect "${mssid}" && sleep 3 && doas iwctl station wlan0 show' \
    wifishow='doas iwctl station wlan0 show' \
    wififorget='read -p "Enter wifi SSID to forget: " mssid && doas iwctl known-networks "${mssid}" forget' \
    apon='doas systemctl start hostapd && sleep 3 && doas bridge link' \
    apoff='doas systemctl stop hostapd && sleep 3 && doas bridge link' \
    displayoff='xset dpms force off' \


# Gentoo Specifics
alias \
    ggit='git --git-dir=/gentoo-config --work-tree=/' \
    ggita='git --git-dir=/gentoo-config --work-tree=/ add' \
    ggits='git --git-dir=/gentoo-config --work-tree=/ status' \
    ggitd='git --git-dir=/gentoo-config --work-tree=/ difftool --no-symlinks --dir-diff' \
    ggitca='read -p "Enter commit message: " && git --git-dir=/gentoo-config --work-tree=/ commit -am "${REPLY}"' \
    gen2crossdevi686='crossdev --overlays "gentoo" --ov-output /var/db/repos/crossdev --portage "--quiet-build -v" --target i686-pc-linux-gnu' \
    gen2update='distcc-config --get-hosts && grep -i -e "makeopts" /etc/portage/make.conf/makeopts && time doas emerge -avuNDbgk --with-bdeps=y --keep-going --quiet-build @world' \
    gen2fetch='distcc-config --get-hosts && grep -i -e "makeopts" /etc/portage/make.conf/makeopts && doas emerge -favuND @world' \
    gen2world='doas emacs -nw /var/lib/portage/world' \
    gen2log='tail -f $(portageq envvar PORTAGE_TMPDIR)/portage/*/*/temp/build.log' \
    gen2use='doas emacs -nw /etc/portage/package.use/allinone' \
    gen2license='doas emacs -nw /etc/portage/package.license/allinone' \
    gen2keyword='doas emacs -nw /etc/portage/package.accept_keywords/allinone' \
    gen2env='doas emacs -nw /etc/portage/package.env/allinone' \
    gen2mask='doas emacs -nw /etc/portage/package.mask/allinone' \
    gen2reposconf='doas emacs -nw /etc/portage/repos.conf/gentoo.conf' \
    gen2inforepos='portageq repos_config /' \
    gen2infoinversemarch='for pkg in $(qlist -I); do grep --invert-match --with-filename -e march=x86-64-v3 /var/db/pkg/${pkg}*/C*FLAGS; done' \
    gen2install='doas emerge -avbgk --quiet-build --keep-going --with-bdeps=y' \
    gen2search='doas emerge -s' \
    gen2rm='doas emerge -ca' \
    gen2rmforce='doas emerge -Ca' \
    gen2elog='cd /var/log/portage/elog/ && ll' \
    gen2ecleanp='doas eclean-dist -p -t2w' \
    gen2eclean='doas eclean-dist -t2w' \
    gen2ecleank='doas eclean-kernel -Aa' \
    gen2lvmsnapshot='read -p "comment for snapshot (underscores only)?" comment && read -p "what size of snapshot (gb digit only)?" gb && doas lvcreate -v -s -L ${gb}G -n "ss_gentoo_$(date +%d_%m_%Y)_${comment}" gnulinux/gentooroot' \
    gen2km='doas emerge -v --quiet-build @module-rebuild' \
    gen2ka='time { eselect kernel list && sleep 2 && make $(portageq envvar MAKEOPTS) && make modules_install && make install && gen2nvidia && doas -u chad cp ./.config ~chad/ansible-homelab/roles/d_gentoo/files/kernel/alienware.config; }' \
    gen2diff='eix-update && eix-diff' \
    gen2sync='doas eix-sync && doas systemctl stop portage-sync.timer' \
    gen2mirrors='mirrorselect -H -4 -D -s 5 -o' \
    eq='equery -h' \
    eqb='equery belongs' \
    eqd='equery depends' \
    eqg='equery depgraph' \
    eqf='equery files' \
    eqh='equery hasuse' \
    eqy='equery keywords' \
    equ='equery uses' \
    eqm='equery meta' \
    eqw='equery which' \
    eql='equery list' \
    eqs='equery size' \


# Program specifics
alias \
    dccstatus='distcc-config --get-hosts && grep -i -e "makeopts" /etc/portage/make.conf/makeopts' \
    dccvps="doas distcc-config --set-hosts '192.168.96.1/5' && distcc-config --get-hosts && doas sed -i -E {s:'MAKEOPTS=\".+\"':'MAKEOPTS=\"-j5 -l8\"':g} /etc/portage/make.conf/makeopts && grep -i -e 'makeopts' /etc/portage/make.conf/makeopts" \
    dcchp="doas distcc-config --set-hosts 'localhost/6 hp.hl.rahil.rocks/3' && distcc-config --get-hosts && doas sed -i -E {s:'MAKEOPTS=\".+\"':'MAKEOPTS=\"-j9 -l8\"':g} /etc/portage/make.conf/makeopts && grep -i -e 'makeopts' /etc/portage/make.conf/makeopts" \
    dccboth="doas distcc-config --set-hosts 'localhost/6 hp.hl.rahil.rocks/3 192.168.96.1/5' && distcc-config --get-hosts && doas sed -i -E {s:'MAKEOPTS=\".+\"':'MAKEOPTS=\"-j14 -l8\"':g} /etc/portage/make.conf/makeopts && grep -i -e 'makeopts' /etc/portage/make.conf/makeopts" \
    dccoff="doas distcc-config --set-hosts 'localhost' && distcc-config --get-hosts && doas sed -i -E {s:'MAKEOPTS=\".+\"':'MAKEOPTS=\"-j6 -l8\"':g} /etc/portage/make.conf/makeopts && grep -i -e 'makeopts' /etc/portage/make.conf/makeopts" \
    dccfix='[ -f $(portageq envvar PORTAGE_TMPDIR)/portage/.distcc/lock/cpu_localhost_0 ] && doas ls -lah --color=auto $(portageq envvar PORTAGE_TMPDIR)/portage/.distcc/lock/cpu_localhost_0 && doas chown portage:portage $(portageq envvar PORTAGE_TMPDIR)/portage/.distcc/lock/cpu_localhost_0 && doas ls -lah --color=auto $(portageq envvar PORTAGE_TMPDIR)/portage/.distcc/lock/cpu_localhost_0' \

    
    
